function groupBy(arr: Array<any>, func: Function) {

  let result = {} as any
  arr.map(i => {
    if (result[func(i)] !== undefined) {
      return result[func(i)].push(i)
    }
    return result[func(i)] = [i]
    
  })
    return result
  }
  
  let oneArr = [1.2, 1.1, 2.3, 0.4]
  let oneFunc = Math.floor
  
  let twoArr = ["one", "two", "three"]
  let twoFunc = (el: string) => el.length
  
  enum Gender {
    Male,
    Female,
  }
  let threeArr = [{ g: Gender.Male, n: "A" },{ g: Gender.Female, n: "B" },{ g: Gender.Female, n: "C" }]
  let threeFunc = (el: {g: Gender, n: string}) => el.g
  
  console.log(groupBy(oneArr, oneFunc))
  console.log(groupBy(twoArr, twoFunc))
  console.log(groupBy(threeArr, threeFunc))