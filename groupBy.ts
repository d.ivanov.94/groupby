function groupBy(a: Array<number | string | LastEl>, b: Function) {
    const result = {} as any

    for (const i in a) {
        result[b(a[i])] = []
    }
    for (let key in result) {
        result[key] = a.filter(i => b(i) === +key)
    }

    return result
}

const arrOne = [1.2, 1.1, 2.3, 0.4]
const funcOne = Math.floor

const arrTwo = ["one", "two", "three"]
const funcTwo = (el: string) => el.length

enum Gender { Male, Female }
type LastEl = { g: Gender, n: string }

const arrThree = [{ g: Gender.Male, n: "A" }, { g: Gender.Female, n: "B" }, { g: Gender.Female, n: "C" }];
const funcThree = (el: LastEl) => el.g

console.log(groupBy(arrOne, funcOne))
console.log(groupBy(arrTwo, funcTwo))
console.log(groupBy(arrThree, funcThree))